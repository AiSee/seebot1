import sc2
from sc2.constants import *
from sc2.units import Units
from sc2.unit import Unit
from sc2.position import Point2, Point3
from pprint import pprint
from typing import Union


class MyBotAI(sc2.BotAI):
    actions = []
    iteration = 0
    prev_units = {}
    enemy_prev_units = {}  # previously seen but not now
    enemy_new_units = {}
    units_harm = []
    units_damage = []
    units_dps = 0
    units_hps = 0

    RESOURCE_SPREAD_THRESHOLD = 8
    HIT_HISTORY_LOOPS = 56  # 2.5 sec
    UNITS_HISTORY_LOOPS = 1000  # 45 sec

    async def do(self, action):
        self.actions.append(action)
        cost = self._game_data.calculate_ability_cost(action.ability)
        self.minerals -= cost.minerals
        self.vespene -= cost.vespene

    async def check_prev_units(self):
        # fields is_hit and hps are available via self.units
        new_units = {}
        for unit in self.state.units:  # all visible units
            unit.is_hit = False
            unit.hps = 0
            if unit.tag in self.prev_units:
                prev_unit = self.prev_units[unit.tag]
                if hasattr(prev_unit, 'hits'):
                    unit.hits = prev_unit.hits
                else:
                    unit.hits = []

                health_delta = unit.health - prev_unit.health + unit.shield - prev_unit.shield
                if health_delta < 0:
                    unit.is_hit = True
                    unit.hits.extend([self.state.game_loop, health_delta])
                    if unit.is_mine:
                        self.units_harm.extend([self.state.game_loop, health_delta])
                    else:
                        self.units_damage.extend([self.state.game_loop, health_delta])

                    # cancel attacked buildings
                    if unit.is_mine and unit.is_structure and not unit.is_ready and \
                            unit.health + unit.shield < unit.hps * 2:
                        await self.do(unit(AbilityId.CANCEL))

                if unit.hits and unit.hits[0] < self.state.game_loop - self.HIT_HISTORY_LOOPS:
                    del unit.hits[0:2]  # remove old info about being hit
                unit.hps = -sum(unit.hits[1::2]) * 22.4 / self.HIT_HISTORY_LOOPS  # harm per second
            new_units[unit.tag] = unit
        self.prev_units = new_units  # this way we forget dead

        while self.units_harm and self.units_harm[0] < self.state.game_loop - self.HIT_HISTORY_LOOPS:
            del self.units_harm[0:2]
        while self.units_damage and self.units_damage[0] < self.state.game_loop - self.HIT_HISTORY_LOOPS:
            del self.units_damage[0:2]
        self.units_hps = -sum(self.units_harm[1::2]) * 22.4 / self.HIT_HISTORY_LOOPS  # harm for all my units per second
        self.units_dps = -sum(self.units_damage[1::2]) * 22.4 / self.HIT_HISTORY_LOOPS  # total damage done per second

    def is_explored(self, pos: Union[Point2, Point3, Unit]) -> bool:
        """ Returns True if you have explored on a grid point. """
        assert isinstance(pos, (Point2, Point3, Unit))
        pos = pos.position.to2.rounded
        return self.state.visibility[pos] != 0

    def check_enemy_prev_units(self):
        for k, v in self.enemy_new_units.items():
            self.enemy_prev_units[k] = v
        self.enemy_new_units = {}
        for unit in self.known_enemy_units.not_structure:
            self.enemy_new_units[unit.tag] = unit
            self.enemy_new_units[unit.tag].last_seen = self.state.game_loop
            if unit.tag in self.enemy_prev_units:  # dict contains only units not visible now
                del self.enemy_prev_units[unit.tag]
        # remove too old information
        for tag in list(self.enemy_prev_units.keys()):
            if self.enemy_prev_units[tag].last_seen < self.state.game_loop - self.UNITS_HISTORY_LOOPS or \
                    self.is_visible(self.enemy_prev_units[tag]):
                del self.enemy_prev_units[tag]

    async def debug_enemy_prev_units(self):
        for tag, unit in self.enemy_prev_units.items():
            self._client.debug_sphere_out(unit.position3d, unit.radius, Point3((255, 0, 0)))
        await self._client.send_debug()

    async def workers_init(self):
        for worker in self.workers:
            if worker.is_gathering:
                closest_mineral_patch = self.state.mineral_field.closest_to(worker)
                await self.do(worker.gather(closest_mineral_patch))

    def can_feed(self, unit_type: UnitTypeId) -> bool:
        """ Checks if you have enough free supply to build the unit """
        return self.supply_left >= self._game_data.units[unit_type.value]._proto.food_required

    async def has_ability(self, ability, unit, ignore_resource_requirements=True):
        abilities = await self.get_available_abilities(unit, ignore_resource_requirements=ignore_resource_requirements)
        return ability in abilities

    async def step(self):
        raise NotImplementedError

    async def double_build_bug_remove(self):
        for building in self.units.structure.filter(lambda b: len(b.orders) > 1):
            if self.state.game_loop % 2 == 0:
                await self.do(building(AbilityId.CANCEL_LAST))

    async def on_step(self, iteration):
        self.actions = []
        self.iteration = iteration
        if iteration == 0:
            self._client.game_step = 3

        await self.double_build_bug_remove()
        await self.check_prev_units()
        self.check_enemy_prev_units()
        # await self.debug_enemy_prev_units()
        await self.step()

        if self.actions:
            await self._client.actions(self.actions, game_data=self._game_data)  # todo: check on success?

    def dbg(self, msg):
        print(self.state.game_loop.__str__() + ": " + msg.__str__())
