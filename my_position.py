from sc2.position import Point2


def sum_point2(a: Point2, b: Point2):
    return Point2((a.x + b.x, a.y + b.y))


def sub_point2(a: Point2, b: Point2):
    return Point2((a.x - b.x, a.y - b.y))


def mul_point2(a: Point2, b: Point2):
    return Point2((a.x * b.x, a.y * b.y))


def in_range_point2(a: Point2, b: Point2, dist=0.1):
    return (a.x - b.x) ** 2 + (a.y - b.y) ** 2 <= dist ** 2
