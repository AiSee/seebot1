import sc2
from sc2 import run_game, maps, Race, Difficulty
from sc2.constants import *
from sc2.player import Bot, Computer
import asyncio
from pprint import pprint


class ProbeRush(sc2.BotAI):
    war_workers = []
    iteration = 0
    actions = []

    async def war_workers_strategy(self):
        def fallback(w):
            home_mineral = self.state.mineral_field.closest_to(self.start_location)
            self.actions.append(w.move(home_mineral))

        if self.iteration == 0:
            self.war_workers = self.workers.tags
        workers = self.workers.tags_in(self.war_workers)

        ok_targets = self.known_enemy_units.not_flying.exclude_type([UnitTypeId.LARVA, UnitTypeId.EGG])
        good_targets = ok_targets.not_structure
        enemy_mineral = self.state.mineral_field.closest_to(self.enemy_start_locations[0])
        if good_targets:  # Slow down for debug
            self._client.game_step = 1
            await asyncio.sleep(0.02)

        for worker in workers:
            if worker.shield + worker.health <= 20 and not worker.is_moving:
                fallback(worker)
            elif worker.shield > 0 or worker.is_moving and \
                    (not good_targets or worker.distance_to(good_targets.closest_to(worker)) > 2):
                if good_targets:
                    self.actions.append(worker.attack(good_targets.closest_to(worker).position))
                elif ok_targets:
                    if not worker.is_attacking:
                        self.actions.append(worker.attack(ok_targets.closest_to(worker).position))
                else:
                    self.actions.append(worker.gather(enemy_mineral))

    async def economics(self):
        cc = self.units(UnitTypeId.NEXUS).first
        if self.can_afford(UnitTypeId.PROBE) and self.supply_left > 0 and cc.noqueue:
            self.actions.append(cc.train(UnitTypeId.PROBE))

        if self.supply_left < 3 and self.can_afford(UnitTypeId.PYLON) and \
                self.already_pending(UnitTypeId.PYLON) < 1:
            bw = self.select_build_worker(cc)
            await self.build(UnitTypeId.PYLON, unit=bw, near=cc.position.towards(self.game_info.map_center, 4))
            mineral = self.state.mineral_field.closest_to(cc)
            self.actions.append(bw.gather(mineral, queue=True))

    async def on_step(self, iteration):
        self.iteration = iteration
        self.actions = []

        await self.war_workers_strategy()
        await self.economics()

        if self.actions:
            await self.do_actions(self.actions)


def main():
    run_game(maps.get("Abyssal Reef LE"), [
        Bot(Race.Protoss, ProbeRush()),
        Computer(Race.Random, Difficulty.Easy)
    ], realtime=False)


if __name__ == '__main__':
    main()
