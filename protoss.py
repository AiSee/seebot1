import sc2
from sc2 import run_game, maps, Race, Difficulty
from sc2.constants import *
from sc2.player import Bot, Computer
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.units import Units
from my_botai import MyBotAI
from my_position import sum_point2, in_range_point2, mul_point2
import asyncio
import random
import math
from pprint import pprint


class ProtossBot(MyBotAI):
    max_probes_count = 70
    defence_radius = 20
    proxy_detected = False
    forge_upgrades = [AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL1,
                      AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL2,
                      AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL3,
                      AbilityId.FORGERESEARCH_PROTOSSSHIELDSLEVEL1,
                      AbilityId.FORGERESEARCH_PROTOSSSHIELDSLEVEL2,
                      AbilityId.FORGERESEARCH_PROTOSSSHIELDSLEVEL3,
                      AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL1,
                      AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL2,
                      AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL3]
    unit_to_ability = {UnitTypeId.ZEALOT: AbilityId.WARPGATETRAIN_ZEALOT,
                       UnitTypeId.ADEPT: AbilityId.TRAINWARP_ADEPT,
                       UnitTypeId.SENTRY: AbilityId.WARPGATETRAIN_SENTRY,
                       UnitTypeId.STALKER: AbilityId.WARPGATETRAIN_STALKER}

    positions_for_pylons = []
    positions_for_buildings = []
    last_gs_casted = 0

    def make_buildings_map(self):
        base_height = self._game_info.terrain_height[self.start_location.rounded]
        min_pos = self.state.mineral_field.closer_than(self.RESOURCE_SPREAD_THRESHOLD, self.start_location).center
        vector = Point2((math.copysign(1, self.start_location.x - min_pos.x),
                         math.copysign(1, self.start_location.y - min_pos.y))).rounded
        # pprint(vector)
        p0 = sum_point2(self.start_location, mul_point2(vector, Point2((4.5, 4.5)))).rounded
        p1 = sum_point2(p0, mul_point2(vector, Point2((4, 6))))
        b0 = sum_point2(p0, mul_point2(vector, Point2((0.5, 2.5))))
        b1 = sum_point2(b0, mul_point2(vector, Point2((0, 3))))
        b2 = sum_point2(p0, mul_point2(vector, Point2((4.5, 0.5))))
        b3 = sum_point2(b2, mul_point2(vector, Point2((0, 3))))
        for y in range(-3, 3):
            for x in range(-3, 3):
                if x < 0 and y < 0:
                    continue
                for pos in [sum_point2(p0, mul_point2(vector, Point2((x * 8, y * 9)))),
                            sum_point2(p1, mul_point2(vector, Point2((x * 8, y * 9))))]:
                    if self._game_info.terrain_height[pos.rounded] == base_height:
                        self.positions_for_pylons.append(pos)
                for pos in [sum_point2(b0, mul_point2(vector, Point2((x * 8, y * 9)))),
                            sum_point2(b1, mul_point2(vector, Point2((x * 8, y * 9)))),
                            sum_point2(b2, mul_point2(vector, Point2((x * 8, y * 9)))),
                            sum_point2(b3, mul_point2(vector, Point2((x * 8, y * 9))))]:
                    if self._game_info.terrain_height[pos.rounded] == base_height:
                        self.positions_for_buildings.append(pos)
        self.positions_for_pylons.sort(key=lambda a: self.start_location.distance_to(a))
        self.positions_for_buildings.sort(key=lambda a: self.start_location.distance_to(a))
        # todo: add more pylons positions
        # pylon for battery
        """ pos = min(self.positions_for_pylons, key=lambda p: p.distance_to(self.main_base_ramp.top_center))
        self.positions_for_pylons.insert(1, pos) """

    async def debug_buildings_map(self):
        for pos in self.positions_for_pylons:
            self._client.debug_box_out(Point3((pos.x - 0.4, pos.y - 0.4, 12)), Point3((pos.x + 0.4, pos.y + 0.4, 14)))
        for pos in self.positions_for_buildings:
            self._client.debug_box_out(Point3((pos.x - 1.4, pos.y - 1.4, 12)), Point3((pos.x + 1.4, pos.y + 1.4, 14)))
        await self._client.send_debug()

    def is_less_than(self, unit: UnitTypeId, count):  # only for buildings
        return self.units(unit).ready.amount + self.already_pending(unit) < count

    def can_train(self, unit: UnitTypeId, qty_needed: int):
        return self.is_less_than(unit, qty_needed) and self.can_afford(unit) and self.can_feed(unit)

    def factory_can_train(self, unit: UnitTypeId, factory: Unit, qty_needed: int, check_queue=True):
        return self.can_train(unit, qty_needed) and (not check_queue or factory.noqueue)

    async def train_if_can(self, unit: UnitTypeId, factory: Unit, qty_needed: int, check_queue=True):
        if self.factory_can_train(unit, factory, qty_needed, check_queue):
            await self.do(factory.train(unit))
            return True
        return False

    # todo: check is safe
    async def take_probe_and_build(self, building: UnitTypeId):
        success = False
        pos_list = self.positions_for_buildings
        if building == UnitTypeId.PYLON:
            pos_list = self.positions_for_pylons
        for pos in pos_list:
            if await self.can_place(building, pos):
                workers = self.workers.idle or self.workers.gathering or self.workers
                if workers.empty:
                    # self.dbg("Warning: Can't find worker to build")
                    return
                for worker in workers.prefer_close_to(pos):
                    if await self._client.query_pathing(worker.position, pos):
                        await self.do(workers.closest_to(pos).build(building, pos))
                        success = True
                        break
                if success:
                    break
        if not success:
            pass  # self.dbg("Warning: Can't build")

    async def build_if_can(self, building: UnitTypeId, qty_needed: int):
        if self.can_afford(building) and self.units(building).amount < qty_needed and \
                self.already_pending(building) < qty_needed:
            await self.take_probe_and_build(building)
            return True
        return False

    async def build_assimilator(self, nexus):
        for gas in self.state.vespene_geyser.closer_than(10, nexus):
            if self.units(UnitTypeId.ASSIMILATOR).closer_than(1.0, gas).empty:
                worker = self.select_build_worker(gas.position, force=True)
                await self.do(worker.build(UnitTypeId.ASSIMILATOR, gas))
                mineral = self.state.mineral_field.closest_to(nexus)
                await self.do(worker.gather(mineral, queue=True))
                break

    def workers_needed(self):
        # 1 worker hides in each assimilator
        cnt = -self.workers.amount
        for nexus in self.units(UnitTypeId.NEXUS):
            if nexus.is_ready:
                cnt += nexus.ideal_harvesters
            else:
                cnt += 16
        for assim in self.units(UnitTypeId.ASSIMILATOR):
            if assim.is_ready:
                cnt += assim.ideal_harvesters - 1
            else:
                cnt += 2
        return cnt

    # todo: better function, sometimes moving worker hangs before assimilator
    async def distribute_workers(self):
        nexuses = self.units(UnitTypeId.NEXUS).ready
        worker_pool = []
        for idle_worker in self.workers.idle:
            mf = self.state.mineral_field.closest_to(nexuses.closest_to(idle_worker))
            await self.do(idle_worker.gather(mf))

        owned_expansions = self.owned_expansions
        for location, townhall in owned_expansions.items():
            workers = self.workers.closer_than(self.RESOURCE_SPREAD_THRESHOLD, location)
            actual = townhall.assigned_harvesters
            ideal = townhall.ideal_harvesters
            excess = actual - ideal
            if actual > ideal:
                worker_pool.extend(workers.random_group_of(min(excess, len(workers))))
                continue
        for g in self.geysers:
            workers = self.workers.closer_than(5, g)
            actual = g.assigned_harvesters
            ideal = g.ideal_harvesters
            excess = actual - ideal
            if actual > ideal:
                worker_pool.extend(workers.random_group_of(min(excess, len(workers))))
                continue

        for g in self.geysers:
            actual = g.assigned_harvesters
            ideal = g.ideal_harvesters
            deficit = ideal - actual

            for x in range(0, deficit):
                if worker_pool:
                    w = worker_pool.pop()
                    if len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_RETURN]:
                        await self.do(w.move(g))
                        await self.do(w.return_resource(queue=True))
                    else:
                        await self.do(w.gather(g))

        for location, townhall in owned_expansions.items():
            actual = townhall.assigned_harvesters
            ideal = townhall.ideal_harvesters

            deficit = ideal - actual
            for x in range(0, deficit):
                if worker_pool:
                    w = worker_pool.pop()
                    mf = self.state.mineral_field.closest_to(townhall)
                    if len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_RETURN]:
                        await self.do(w.move(townhall))
                        await self.do(w.return_resource(queue=True))
                        await self.do(w.gather(mf, queue=True))
                    else:
                        await self.do(w.gather(mf))

    async def get_free_warpgate(self):
        for wg in self.units(UnitTypeId.WARPGATE).ready:
            if await self.has_ability(AbilityId.WARPGATETRAIN_ZEALOT, wg, ignore_resource_requirements=True):
                return wg
        return None

    def choose_unit_to_train(self):
        if not self.units(UnitTypeId.CYBERNETICSCORE).ready and self.can_train(UnitTypeId.ZEALOT, 4):
            return UnitTypeId.ZEALOT
        if self.can_train(UnitTypeId.STALKER, 100):
            return UnitTypeId.STALKER
        if self.can_train(UnitTypeId.SENTRY, 2):
            return UnitTypeId.SENTRY
        # todo: more strats for other races and units they use
        if self.enemy_race in [Race.Terran, Race.Zerg]:  # less zealots, more adepts
            if self.can_train(UnitTypeId.ZEALOT, (self.supply_used / 20).__round__()):
                return UnitTypeId.ZEALOT
            elif self.can_train(UnitTypeId.ADEPT, (self.supply_used / 10).__round__()):
                return UnitTypeId.ADEPT
        #  too much minerals and no gas spend them on zealots
        if self.can_train(UnitTypeId.ZEALOT, (self.supply_used / 10).__round__()) or \
                self.minerals >= 250 and self.vespene < 25 and self.can_train(UnitTypeId.ZEALOT, 50):
            return UnitTypeId.ZEALOT
        return None

    async def strategy(self):
        if self.units.structure.amount == 1:
            last_building = self.units.structure.first
            if last_building.health + last_building.shield < last_building.hps * 2:
                await self.chat_send("(gg)")
                await self._client.leave()
                return

        # Workers
        await self.distribute_workers()
        nexuses = self.units(UnitTypeId.NEXUS)
        if nexuses.empty:  # banzai!
            for worker in self.workers:
                await self.do(worker.attack(self.enemy_start_locations[0]))
            return
        nexus = nexuses.first

        if self.iteration == 0:
            await self.do(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, nexus))
            await self.do(nexus.train(UnitTypeId.PROBE))
            await self.workers_init()
            return

        if self.workers_needed() > 0 and await self.train_if_can(UnitTypeId.PROBE, nexus, self.max_probes_count):
            return  # Here and after: no more actions if one done

        # Chronoboost
        for booster in self.units(UnitTypeId.NEXUS).ready:
            if await self.has_ability(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, booster) and booster.energy >= 50:
                for building in self.units.of_type([UnitTypeId.CYBERNETICSCORE, UnitTypeId.FORGE,
                                                    UnitTypeId.TWILIGHTCOUNCIL]).ready:
                    if not building.noqueue and not building.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                        time = self._game_data.abilities[building.orders[0].ability.id.value].cost.time
                        if not time:  # can't get time for ability
                            time = 136 * 22.4  # average time for upgrades which abilities has no time
                        if (1 - building.orders[0].progress) * time / 22.4 < 25:
                            continue  # skip if less than 25 seconds left
                        await self.do(booster(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, building))
                        return

        # Upgrades
        if not self.proxy_detected:
            cc = self.units(UnitTypeId.CYBERNETICSCORE).ready.noqueue
            if cc.exists and await self.has_ability(AbilityId.RESEARCH_WARPGATE, cc.first):
                if self.can_afford(AbilityId.RESEARCH_WARPGATE):
                    await self.do(cc.first(AbilityId.RESEARCH_WARPGATE))
                # else: do nothing, wait for resources
                return

            forge = self.units(UnitTypeId.FORGE).ready.noqueue
            if forge.exists:
                for ability in self.forge_upgrades:
                    if await self.has_ability(ability, forge.first):
                        if self.can_afford(ability):
                            await self.do(forge.first(ability))
                        return

            tc = self.units(UnitTypeId.TWILIGHTCOUNCIL).ready.noqueue
            if tc.exists:
                if await self.has_ability(AbilityId.RESEARCH_CHARGE, tc.first):
                    if self.can_afford(AbilityId.RESEARCH_CHARGE):
                        await self.do(tc.first(AbilityId.RESEARCH_CHARGE))
                    # else: do nothing, wait for resources
                    return
                if await self.has_ability(AbilityId.RESEARCH_BLINK, tc.first):
                    if self.can_afford(AbilityId.RESEARCH_BLINK):
                        await self.do(tc.first(AbilityId.RESEARCH_BLINK))
                    # else: do nothing, wait for resources
                    return
                if await self.has_ability(AbilityId.RESEARCH_ADEPTRESONATINGGLAIVES, tc.first) and \
                        self.units.of_type(UnitTypeId.ADEPT).amount >= 4:
                    if self.can_afford(AbilityId.RESEARCH_ADEPTRESONATINGGLAIVES):
                        await self.do(tc.first(AbilityId.RESEARCH_ADEPTRESONATINGGLAIVES))
                    # else: do nothing, wait for resources
                    return

        # Buildings
        if self.supply_left < 3 + self.supply_used / 10 and self.can_afford(UnitTypeId.PYLON) and \
                self.already_pending(UnitTypeId.PYLON) < self.supply_used / 50 and self.supply_cap < 200:
            await self.take_probe_and_build(UnitTypeId.PYLON)
            return

        if self.units(UnitTypeId.PYLON).ready.exists and self.is_less_than(UnitTypeId.WARPGATE, 2) and \
                await self.build_if_can(UnitTypeId.GATEWAY, 2):
            return

        if (self.workers.amount == 16 and self.can_afford(UnitTypeId.ASSIMILATOR) and
            self.units(UnitTypeId.ASSIMILATOR).empty and self.already_pending(UnitTypeId.ASSIMILATOR) == 0) or \
                (self.workers.amount == 19 and self.can_afford(UnitTypeId.ASSIMILATOR) and
                 self.units(UnitTypeId.ASSIMILATOR).amount == 1 and self.already_pending(UnitTypeId.ASSIMILATOR) == 0):
            await self.build_assimilator(nexus)
            return

        # 2 gateways ready and building
        if not self.is_less_than(UnitTypeId.GATEWAY, 2) and (self.units(UnitTypeId.GATEWAY).noqueue.empty or
                                                             self.units.of_type(UnitTypeId.ZEALOT).exists) and \
                await self.build_if_can(UnitTypeId.CYBERNETICSCORE, 1):
            return

        """if not self.is_less_than(UnitTypeId.GATEWAY, 3) and await self.build_if_can(UnitTypeId.FORGE, 1):
            return"""
        if self.units(UnitTypeId.CYBERNETICSCORE).ready.exists and self.is_less_than(UnitTypeId.TWILIGHTCOUNCIL, 1):
            await self.build_if_can(UnitTypeId.TWILIGHTCOUNCIL, 1)
            # else: do nothing, wait for resources
            return

        if self.already_pending_upgrade(UpgradeId.WARPGATERESEARCH) == 1:
            condition = self.units(UnitTypeId.WARPGATE).amount >= 2 and await self.get_free_warpgate() is None
        else:
            condition = self.units(UnitTypeId.GATEWAY).amount >= 2 and self.units(UnitTypeId.GATEWAY).noqueue.empty
        if self.units(UnitTypeId.PYLON).ready.exists and self.already_pending(UnitTypeId.GATEWAY) <= 1 and \
                condition and self.can_afford(UnitTypeId.GATEWAY) and self.units(UnitTypeId.CYBERNETICSCORE).exists:
            await self.take_probe_and_build(UnitTypeId.GATEWAY)
            return

        # Units
        new_unit = self.choose_unit_to_train()
        if new_unit is not None:
            gws = self.units(UnitTypeId.GATEWAY).ready.noqueue
            if gws.exists:
                gw = gws.first
                if await self.has_ability(AbilityId.MORPH_WARPGATE, gw):
                    await self.do(gw(AbilityId.MORPH_WARPGATE))
                    return
                if await self.train_if_can(new_unit, gw, 100):
                    return

            wg = await self.get_free_warpgate()
            if wg is not None:
                warpers = self.units.of_type([UnitTypeId.WARPGATE, UnitTypeId.NEXUS])
                for pylon in self.units(UnitTypeId.PYLON).ready.prefer_close_to(self.enemy_start_locations[0]):
                    if warpers.closer_than(5, pylon).exists:  # this pylon is ok for fast warp
                        # random here because find_placement don't check already warping units
                        pos = await self.find_placement(self.unit_to_ability[new_unit],
                                                        pylon.position.random_on_distance(1.5),
                                                        random_alternative=True, max_distance=5, placement_step=1)
                        if pos is not None:
                            await self.do(wg.warp_in(new_unit, pos))
                            return

    async def probe_defence(self, army):
        # don't try to destroy gateway with probes. Pylon is enough
        targets = self.known_enemy_units.not_flying.exclude_type(UnitTypeId.GATEWAY) \
            .closer_than(self.defence_radius, self.start_location)
        if targets.empty or self.workers.empty:
            attacking_workers = self.workers.filter(lambda u: u.is_attacking)
            for aw in attacking_workers:
                await self.do(aw.stop())
            return False  # no enemies, or no workers act as always

        enemy_dps = 0
        for unit in targets:
            enemy_dps += unit.ground_dps
        worker_dps = self.workers.first.ground_dps

        home_mineral = self.state.mineral_field.closest_to(self.start_location)
        army |= self.workers.filter(lambda u: u.is_attacking)
        army |= self.workers.sorted(lambda u: u.shield + u.health +
                                              u.distance_to(self.start_location) / 10, reverse=True) \
            .take(max(0, round(enemy_dps / worker_dps - army.amount + targets.structure.amount * 4)), require_all=False)

        for unit in army:
            if targets.not_structure.empty:
                target = targets.closest_to(unit)
            else:
                target = targets.closest_to(unit).position
            enemy_is_far = self.start_location.distance_to(target) > self.defence_radius - 1
            low_health = unit.shield + unit.health <= 20
            if (low_health or enemy_is_far) and unit.is_attacking and unit.distance_to(home_mineral) > 2:
                if unit.type_id == UnitTypeId.PROBE:
                    await self.do(unit.gather(home_mineral))
                else:
                    await self.do(unit.move(home_mineral))
                continue
            if not enemy_is_far and not low_health:
                await self.do(unit.attack(target))
        return True

    async def tactics(self):
        ok_flying = self.known_enemy_units.exclude_type([UnitTypeId.LARVA, UnitTypeId.EGG])
        ok_ground = ok_flying.not_flying
        good_flying = ok_flying.not_structure
        good_ground = ok_ground.not_structure
        army = self.units.of_type([UnitTypeId.ZEALOT, UnitTypeId.ADEPT, UnitTypeId.STALKER, UnitTypeId.SENTRY])
        gs_active = army.filter(lambda u: u.has_buff(BuffId.GUARDIANSHIELD)).exists or \
                    self.last_gs_casted + 20 > self.state.game_loop

        if self.state.game_loop == 1008:  # 45 sec
            workers = self.workers.idle or self.workers.gathering or self.workers
            if workers.exists:
                scout = workers.first
                await self.do(scout.move(self.start_location))
                els = sorted(self.expansion_locations.keys(),
                             key=lambda p: p.distance_to(self.main_base_ramp.bottom_center), reverse=True)
                els.insert(0, els[-1].towards(self.start_location, -6))  # check natural twice
                for pos in els:
                    if pos.distance_to(self.main_base_ramp.top_center) > 60:
                        continue
                    await self.do(scout.move(pos, queue=True))
        if self.known_enemy_units.structure.closer_than(70, self.main_base_ramp.top_center).exists:
            self.proxy_detected = True
            min_units_count = 15
            self.defence_radius = 70
            """if self.units(UnitTypeId.CYBERNETICSCORE).ready.exists and \
                    self.is_less_than(UnitTypeId.SHIELDBATTERY, 2) and \
                    self.can_afford(UnitTypeId.SHIELDBATTERY):
                await self.build(UnitTypeId.SHIELDBATTERY, self.main_base_ramp.top_center.towards(
                    self.units.of_type(UnitTypeId.PYLON).closest_to(self.main_base_ramp.top_center).position, 4))"""
        else:
            self.proxy_detected = False
            min_units_count = 2
            self.defence_radius = 20

        if army.amount <= 2:
            if await self.probe_defence(army):
                return

        enemies = self.known_enemy_units | self.enemy_prev_units.values()
        for unit in army:
            my_dps = enemy_dps = 0
            for u in army.closer_than(10, unit):
                my_dps += u.ground_dps
            if enemies.ready.exists:
                for u in enemies.ready.closer_than(20, unit):
                    enemy_dps += u.ground_dps

            # army is big enough | we are winning | enemy is near
            attack_ground = army.amount >= min_units_count and my_dps >= enemy_dps or \
                            self.units_dps > self.units_hps or \
                            ok_ground.closer_than(self.defence_radius, self.start_location)
            attack_flying = attack_ground or ok_flying.closer_than(self.defence_radius, self.start_location)

            attack = attack_ground
            if unit.can_attack_air:
                attack = attack_flying
            if not gs_active and unit.type_id == UnitTypeId.SENTRY and self.units_hps > 0 and \
                    await self.can_cast(unit, AbilityId.GUARDIANSHIELD_GUARDIANSHIELD):
                await self.do(unit(AbilityId.GUARDIANSHIELD_GUARDIANSHIELD))
                gs_active = True
                self.last_gs_casted = self.state.game_loop
                continue

            target = self.main_base_ramp.top_center.towards(self.start_location, 3)
            if attack:
                target = self.enemy_start_locations[0]
                # explore all expansions if enemy main is destroyed
                if self.is_explored(target):
                    possible_locations = self.state.mineral_field.sorted_by_distance_to(self.start_location)
                    target = possible_locations[hash(str(unit.tag)) % len(possible_locations)].position
                # check for available targets
                if good_flying.exists and unit.can_attack_air:
                    target = good_flying.closest_to(unit).position
                elif good_ground.exists:
                    target = good_ground.closest_to(unit).position
                if ok_flying.exists and unit.can_attack_air:
                    target = ok_flying.closest_to(unit).position
                elif ok_ground.exists:
                    target = ok_ground.closest_to(unit).position

            # find Point2 of order, if available
            order_position = None
            if isinstance(unit.order_target, int):
                enemy = self.known_enemy_units.find_by_tag(unit.order_target)
                if enemy is not None:
                    order_position = enemy.position
            else:
                order_position = unit.order_target

            need_move = False
            # we are winning/chasing, this is ranged unit, weapons recharging, not too close
            # OR no enemies around OR retreating
            if (self.units_hps * 2 < self.units_dps and unit.ground_range > 4 and
                unit.weapon_cooldown > 2 and unit.distance_to(target) > 4) or \
                    ok_flying.empty or ok_flying.closest_to(unit).distance_to(unit) > unit.sight_range or \
                    (not attack and (unit.weapon_cooldown > 2 or unit.ground_range < 4 or
                                     unit.type_id is UnitTypeId.SENTRY)):
                need_move = True

            # zealots sould not retreat from faster units
            if need_move and unit.type_id is UnitTypeId.ZEALOT:
                if self.known_enemy_units.not_flying.closer_than(5, unit) \
                        .filter(lambda enemy: enemy.movement_speed > unit.movement_speed).exists:
                    need_move = False

            if await self.has_ability(AbilityId.EFFECT_BLINK_STALKER, unit, ignore_resource_requirements=False) and \
                    unit.distance_to(target) >= 8:
                if not need_move:
                    target = target.towards(unit.position, 6)
                await self.do(unit(AbilityId.EFFECT_BLINK_STALKER, target))
                continue  # blink, and we don't need more orders

            # surrently no target, maybe idle and new target is far
            if (not unit.is_attacking and ((not isinstance(order_position, Point2) and
                                            not in_range_point2(unit.position, target, dist=unit.sight_range / 2)) or
                                           (isinstance(order_position,
                                                       Point2) and  # has target, and new target is not same
                                            not in_range_point2(order_position, target, dist=1)))) or \
                    (need_move and unit.is_attacking) or (not need_move and unit.is_moving):  # change order type
                if need_move:
                    await self.do(unit.move(target))
                else:
                    await self.do(unit.attack(target))

    async def step(self):
        if self.iteration == 0:
            self.make_buildings_map()
        if self.iteration == 1:
            await self.chat_send("SeeBot v1.4.2 (glhf)")
        """if self.iteration % 50 == 0:
            pprint([self.units_dps, self.units_hps])"""

        # await self.debug_buildings_map()

        # pprint([self.state.game_loop, self.get_time(), gw.first])
        await self.strategy()
        await self.tactics()


def main():
    map_name = random.choice(
        ["AcidPlantLE", "BlueshiftLE", "CeruleanFallLE", "DreamcatcherLE", "FractureLE", "LostAndFoundLE",
         "ParaSiteLE"])
    run_game(maps.get(map_name), [
        Bot(Race.Protoss, ProtossBot()),
        Computer(Race.Random, Difficulty.VeryHard)
    ], realtime=False)


if __name__ == '__main__':
    main()
