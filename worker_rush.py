import sc2
from sc2 import run_game, maps, Race, Difficulty
from sc2.constants import *
from sc2.player import Bot, Computer
from sc2.units import Units
from sc2.position import Point2
from pprint import pprint
import asyncio


class WorkerRushBot(sc2.BotAI):
    def __init__(self):
        self.war_workers = {}
        self.iteration = 0
        self.actions = []

    async def war_workers_strategy(self):
        def fallback(w):
            home_mineral = self.state.mineral_field.closest_to(self.start_location)
            self.actions.append(w.move(home_mineral))

        if self.iteration == 0:
            self.war_workers['workers'] = self.workers.tags
            self.war_workers['attackers'] = self.workers.tags
            self.war_workers['kamikaze'] = []
        workers = self.workers.tags_in(self.war_workers['workers'])
        attackers = self.workers.tags_in(self.war_workers['attackers'])
        kamikaze = self.workers.tags_in(self.war_workers['kamikaze'])

        ok_targets = self.known_enemy_units.not_flying.exclude_type([UnitTypeId.LARVA, UnitTypeId.EGG])
        good_targets = ok_targets.not_structure

        for worker in attackers:
            if worker.health <= 15:
                self.war_workers['attackers'].remove(worker.tag)
                self.war_workers['kamikaze'].append(worker.tag)
                fallback(worker)
        for worker in kamikaze:
            if worker.is_idle or (worker.is_moving and (len(good_targets) == 0 or
                                                        worker.distance_to(good_targets.closest_to(worker)) > 3)):
                if good_targets:
                    self.actions.append(worker.attack(good_targets.closest_to(worker)))

        # recalculate group
        attackers = self.workers.tags_in(self.war_workers['attackers'])

        if good_targets:
            for worker in attackers:
                self.actions.append(worker.attack(good_targets.closest_to(worker).position))
        elif ok_targets:
            for worker in workers:
                if not worker.is_attacking:
                    self.actions.append(worker.attack(ok_targets.closest_to(worker).position))
        else:
            enemy_mineral = self.state.mineral_field.closest_to(self.enemy_start_locations[0])
            for worker in workers:
                self.actions.append(worker.gather(enemy_mineral))

    async def economics(self):
        cc = self.units(UnitTypeId.COMMANDCENTER).first
        if self.can_afford(UnitTypeId.SCV) and self.supply_left > 0 and cc.noqueue:
            self.actions.append(cc.train(UnitTypeId.SCV))

        if self.supply_left < 3 and self.can_afford(UnitTypeId.SUPPLYDEPOT) and \
                self.already_pending(UnitTypeId.SUPPLYDEPOT) < 1:
            bw = self.select_build_worker(cc)
            await self.build(UnitTypeId.SUPPLYDEPOT, unit=bw, near=cc.position.towards(self.game_info.map_center, 4))
            mineral = self.state.mineral_field.closest_to(cc)
            self.actions.append(bw.gather(mineral, queue=True))

    async def on_step(self, iteration):
        self.iteration = iteration
        self.actions = []
        if self.iteration > 110:
            self._client.game_step = 1
            await asyncio.sleep(0.02)

        await self.war_workers_strategy()
        await self.economics()

        if self.actions:
            await self.do_actions(self.actions)


def main():
    run_game(maps.get("Abyssal Reef LE"), [
        Bot(Race.Terran, WorkerRushBot()),
        Computer(Race.Random, Difficulty.VeryEasy)
    ], realtime=False)


if __name__ == '__main__':
    main()
